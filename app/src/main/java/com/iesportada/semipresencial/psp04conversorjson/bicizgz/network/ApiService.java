package com.iesportada.semipresencial.psp04conversorjson.bicizgz.network;

import com.iesportada.semipresencial.psp04conversorjson.bicizgz.model.Listado;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Headers;

public interface ApiService {
    @Headers("Content-Type: application/json")
    @GET("urbanismo-infraestructuras/estacion-bicicleta.json?rf=html&start=0&rows=130")
    Call<Listado> getResults();
}
