package com.iesportada.semipresencial.psp04conversorjson.bicizgz;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.view.View;

import com.iesportada.semipresencial.psp04conversorjson.R;
import com.iesportada.semipresencial.psp04conversorjson.databinding.ActivityDetailBinding;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.format.DateTimeFormatter;
import java.util.Date;

/**
 * @author RNarvaiza
 *
 */
public class DetailActivity extends AppCompatActivity {

    private static final String TAG = "DetailActivity";
    private ActivityDetailBinding binding;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);
        binding = ActivityDetailBinding.inflate(getLayoutInflater());
        getSupportActionBar().hide();
        View v = binding.getRoot();
        setContentView(v);
        Log.d(TAG, "onCreate: Started");

        getIncomingIntent();
    }

    //Get the intent and his data.
    private void getIncomingIntent(){
        Log.d(TAG, "getIncomingIntent: Checking for incoming intents.");
        if(getIntent().hasExtra("title")
                && getIntent().hasExtra("anclajes")
                && getIntent().hasExtra("bicis")
                && getIntent().hasExtra("estado")
                && getIntent().hasExtra("date")){
            String titulo = getIntent().getStringExtra("title");
            int anclajes = getIntent().getIntExtra("anclajes", 0);
            int bicis = getIntent().getIntExtra("bicis", 0);
            String estado = getIntent().getStringExtra("estado");
            String date = getIntent().getStringExtra("date");

            setDetailData(titulo,estado, date, anclajes, bicis);
        }
    }

    //Layout population with intent data.
    private void setDetailData(String titulo, String estado, String date, int anclajes, int bicis){
        binding.textViewAnclajesDispo.setText(String.valueOf(anclajes));
        binding.TVBicisDispo.setText(String.valueOf(bicis));
        binding.textViewNombreParking.setText(titulo);
        if (estado.equalsIgnoreCase("OPN")){
            binding.textViewEstadoParking.setText("Disponible");
        }
        else
            binding.textViewEstadoParking.setText("No disponible");

        binding.textViewLastUpdate.setText(dateFormat(date).toString());
    }


//Change the date format to more comprehensive date.
    private Date dateFormat(String date){
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
        Date newDate = null;
        try {
            newDate = sdf.parse(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return newDate;
    }
}