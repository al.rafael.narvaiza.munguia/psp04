package com.iesportada.semipresencial.psp04conversorjson.Conversor.utils;

/**
 * @author Rafa Narvaiza
 * Esta clase contiene un único método que nos construye un String del archivo JSON leido.
 */

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;

public class ReadFiles {



//El método lector nos devolverá un String con la lectura del archivo JSON descargado.
    public String readRawFile() throws IOException {

        StringBuilder s = new StringBuilder();
        String line = "";
        FileInputStream in = new FileInputStream("/storage/emulated/0/eurorates.json");
        BufferedReader br = new BufferedReader(new InputStreamReader(in));
        while (((line = br.readLine()) != null) && !line.isEmpty()){
            s.append(line + "\n");
        }
        in.close();

        return s.toString();
    }

}
