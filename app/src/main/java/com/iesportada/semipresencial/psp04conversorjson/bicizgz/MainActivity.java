package com.iesportada.semipresencial.psp04conversorjson.bicizgz;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;
import com.iesportada.semipresencial.psp04conversorjson.R;
import com.iesportada.semipresencial.psp04conversorjson.bicizgz.adapter.ClickListener;
import com.iesportada.semipresencial.psp04conversorjson.bicizgz.adapter.RecyclerTouchListener;
import com.iesportada.semipresencial.psp04conversorjson.bicizgz.adapter.ResultAdapter;
import com.iesportada.semipresencial.psp04conversorjson.bicizgz.model.Listado;
import com.iesportada.semipresencial.psp04conversorjson.bicizgz.model.Result;
import com.iesportada.semipresencial.psp04conversorjson.bicizgz.network.ApiAdapter;
import com.iesportada.semipresencial.psp04conversorjson.databinding.ActivityMain3Binding;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * @author RNarvaiza
 * @MainActivity encargada de hacer la petición para obtener la información de la APIRest y tratarla y pasar datos a la actividad detalle.
 */

public class MainActivity extends AppCompatActivity implements Callback<Listado> {
    private ActivityMain3Binding binding;
    private ResultAdapter adapter;
    private Call<Listado> call;
    private ArrayList<Result> resultados;
    private List<Listado> estaciones;
    private SwipeRefreshLayout swipeRefreshLayout;
    Context context;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main3);
        binding = ActivityMain3Binding.inflate(getLayoutInflater());
        View v = binding.getRoot();
        setContentView(v);

        getSupportActionBar().setTitle("BiziZgZ");

        context = getApplicationContext();

        adapter = new ResultAdapter();

        binding.recyclerView.setAdapter(adapter);

        //SwipeRefreshLayout intialization and color schema declaration.
        swipeRefreshLayout=binding.swipeRefreshLayout;
        swipeRefreshLayout.setColorSchemeResources(R.color.design_default_color_primary);
        swipeRefreshLayout.setProgressBackgroundColorSchemeResource(R.color.material_on_surface_emphasis_medium);

        binding.recyclerView.setLayoutManager(new LinearLayoutManager(this));


        populateList();

        //OnItemClickListener to give through the activities info to populate detail activity.
        binding.recyclerView.addOnItemTouchListener(new RecyclerTouchListener(this, binding.recyclerView, new ClickListener() {
            @Override
            public void onClick(View view, final int position) {

                Intent intent = new Intent(context, DetailActivity.class);
                intent.putExtra("title", resultados.get(position).getTitle());
                intent.putExtra("anclajes", resultados.get(position).getAnclajes());
                intent.putExtra("bicis", resultados.get(position).getBicisDisponibles());
                intent.putExtra("estado", resultados.get(position).getEstado());
                intent.putExtra("date", resultados.get(position).getLastUpdated());
                startActivity(intent);

            }

        }));

        //SwipeRefresh implementation.
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                populateList();
            }
        });
    }

    //Function declared to manage de api call.
    public void populateList(){
        call = ApiAdapter.getInstance().getResults();
        call.enqueue(this);
    }

    private void showMessage(String s) {
        Toast.makeText(this, s, Toast.LENGTH_SHORT).show();
    }


    // Populate collection with api data source.
    @Override
    public void onResponse(Call<Listado> call, Response<Listado> response) {
        if(response.isSuccessful()){
            estaciones = Collections.singletonList(response.body());
            for( Listado e : estaciones){
                resultados=e.getResult();
                adapter.setParkings(resultados);
                binding.recyclerView.setAdapter(adapter);
            }
            showMessage("Parkings actualizados");
            swipeRefreshLayout.setRefreshing(false);
        }
        else {
            StringBuilder message = new StringBuilder();
            message.append("Error en la descarga " + response.code());
            if(response.body() != null){
                message.append("\n" + response.body());
            }
            if(response.errorBody() != null){
                try{
                    message.append("\n" + response.errorBody().string());
                }catch (IOException e){
                    e.printStackTrace();
                }
                showMessage(message.toString());
            }
        }
    }

    @Override
    public void onFailure(Call<Listado> call, Throwable t) {

    }

}