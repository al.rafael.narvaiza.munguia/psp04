package com.iesportada.semipresencial.psp04conversorjson.Conversor.model;

/**
 * @author Rafa Narvaiza
 * Este POJO declara la estructura del objeto JSON secundario, el que contiene los diferentes valores del cambio del EURO frente a otras divisas.
 */

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Rates implements Serializable {

    @SerializedName("USD")
    @Expose
    private Double usd;
    @SerializedName("AUD")
    @Expose
    private Double aud;
    @SerializedName("CAD")
    @Expose
    private Double cad;
    @SerializedName("PLN")
    @Expose
    private Double pln;
    @SerializedName("MXN")
    @Expose
    private Double mxn;

    public Double getUsd() {
        return usd;
    }

    public void setUsd(Double usd) {
        this.usd = usd;
    }

    public Double getAud() {
        return aud;
    }

    public void setAud(Double aud) {
        this.aud = aud;
    }

    public Double getCad() {
        return cad;
    }

    public void setCad(Double cad) {
        this.cad = cad;
    }

    public Double getPln() {
        return pln;
    }

    public void setPln(Double pln) {
        this.pln = pln;
    }

    public Double getMxn() {
        return mxn;
    }

    public void setMxn(Double mxn) {
        this.mxn = mxn;
    }

}