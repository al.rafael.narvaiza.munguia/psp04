package com.iesportada.semipresencial.psp04conversorjson.Conversor;

/**
 * @author Rafa Narvaiza
 * Conversor con un IntentService que se encarga de descargar con OKHttp un JSON de una api verificada que nos ofrece el tipo de cambio del EURO diario.
 * La clase main se encarga de levantar la actividad principal y el intent del servicio de descarga, gestionar los permisos y gestionar la lectura del JSON y su posterior parseo a objeto para la extracción de la información.
 */

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import android.Manifest;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import com.iesportada.semipresencial.psp04conversorjson.Conversor.model.RatesEnquiry;
import com.iesportada.semipresencial.psp04conversorjson.Conversor.network.DownloadIntentService;
import com.iesportada.semipresencial.psp04conversorjson.Conversor.utils.ReadFiles;
import com.iesportada.semipresencial.psp04conversorjson.R;
import com.iesportada.semipresencial.psp04conversorjson.databinding.ActivityMainBinding;

import java.io.IOException;

public class MainActivity extends AppCompatActivity {

    ReadFiles readFiles = new ReadFiles();
    private static final int REQUEST_CONNECT = 1;
    public static final String WEB = "http://api.exchangeratesapi.io/v1/latest?access_key=4e928c86f666bc3ad61c7cd225743aeb&symbols=USD,AUD,CAD,PLN,MXN&format=1";
    public static final String ACTION_RESP = "RESPUESTA_DESCARGA";
    private ActivityMainBinding binding;
    private static double rate = 0;
    private double dolarAmount;
    private double euroAmount;
    IntentFilter intentFilter;
    BroadcastReceiver broadcastReceiver;
    String content;
    RatesEnquiry ratesEnquiry;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        binding = ActivityMainBinding.inflate(getLayoutInflater());
        getSupportActionBar().setTitle("JSON Currency converter");
        View view = binding.getRoot();
        setContentView(view);
        intentFilter = new IntentFilter(ACTION_RESP);
        intentFilter.addCategory(Intent.CATEGORY_DEFAULT);
        broadcastReceiver = new ReceptorOperacion();

        //Lanzamos el intentService en el onCreate. De esta forma estamos creando un servicio que se inicia en todas las ejecuciones para manejar la descarga del JSON.
        intentService();
    }

    private void intentService(){
        if (comprobarPermiso()){
            Intent i = new Intent(MainActivity.this, DownloadIntentService.class);
            i.putExtra("web", WEB);
            startService(i);
        }
        try {
            Thread.sleep(300);
            stopService(new Intent(MainActivity.this, DownloadIntentService.class));
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

    }

    @Override
    public void onResume(){
        super.onResume();
        //---registrar el receptor ---
        registerReceiver(broadcastReceiver, intentFilter);
    }

    @Override
    public void onPause(){
        super.onPause();
        //--- anular el registro del recpetor ---
        unregisterReceiver(broadcastReceiver);
    }


    public class ReceptorOperacion extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {
            String respuesta = intent.getStringExtra("resultado");
            manageJsonInputData();
        }
    }

    private boolean comprobarPermiso() {
        //https://developer.android.com/training/permissions/requesting?hl=es-419
        String permiso = Manifest.permission.WRITE_EXTERNAL_STORAGE;
        // Manifest.permission.INTERNET
        boolean concedido = false;
        // comprobar los permisos
        if (ActivityCompat.checkSelfPermission(this, permiso) != PackageManager.PERMISSION_GRANTED) {
            // pedir los permisos necesarios, porque no están concedidos
            if (ActivityCompat.shouldShowRequestPermissionRationale(this, permiso)) {
                concedido = false;
            } else {
                ActivityCompat.requestPermissions(this, new String[]{permiso}, REQUEST_CONNECT);
                // Cuando se cierre el cuadro de diálogo se ejecutará onRequestPermissionsResult
            }
        } else {
            concedido = true;
        }
        return concedido;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        String permiso = Manifest.permission.WRITE_EXTERNAL_STORAGE;
        //Manifest.permission.INTERNET;
        // chequeo los permisos de nuevo
        if (requestCode == REQUEST_CONNECT)
            if (ActivityCompat.checkSelfPermission(this, permiso) == PackageManager.PERMISSION_GRANTED)
                // permiso concedido
                startService(new Intent(MainActivity.this, DownloadIntentService.class));
            else
                // no hay permiso
                showToast("No se ha concedido permiso para conectarse a Internet");
    }


    public void manageJsonInputData(){
        Gson gson;
        try{
            //Llenamos un string con la lectura del JSON.
            content = readFiles.readRawFile();
            gson = new Gson();
            //Parseamos el JSON a object.
            ratesEnquiry = gson.fromJson(content, RatesEnquiry.class);
            //Cogemos el valor del dolar y lo seteamos en la variable que sirve de tipo de cambio.
            setRate(ratesEnquiry.getRates().getUsd());

            showToast("Thanks to " + WEB.substring(7,30) + " for providing daily exchange rates. \n On " + ratesEnquiry.getDate() + " Euro value is : " +getRate() + " Dolar.");

        } catch (IOException | JsonSyntaxException e) {
            System.out.println(e.getMessage());
        }
    }

    public void onDolarClick(View view){

        binding.editTextDolar.setInputType(InputType.TYPE_CLASS_NUMBER |
                InputType.TYPE_NUMBER_FLAG_DECIMAL |
                InputType.TYPE_NUMBER_FLAG_SIGNED);

        binding.editTextDolar.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {

                if (binding.editTextDolar.getText().toString().equals("NaN") || binding.editTextDolar.getText().toString().equals("")){
                    showToast("Please input a number");
                    binding.editTextDolar.setText("0");
                }else {
                    if(Double.valueOf(binding.editTextDolar.getText().toString()) != 0){
                        try{
                            if (binding.editTextDolar.hasFocus()){
                                setDolarAmount(Double.valueOf(binding.editTextDolar.getText().toString()));
                                binding.editTextEuro.setText(fromDolarToEuro());
                            }
                        } catch (NumberFormatException nfe){
                            showToast(nfe.getMessage());
                        }
                    }
                    else{
                        showToast("Introduzca un valor distinto de 0");
                    }
                }
            }
        });

    }


    public void onEuroClick(View view){

        binding.editTextEuro.setInputType(InputType.TYPE_CLASS_NUMBER |
                InputType.TYPE_NUMBER_FLAG_DECIMAL |
                InputType.TYPE_NUMBER_FLAG_SIGNED); //Tratamos el input para evitar cualquier caracter que no sea digito.

        binding.editTextEuro.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {


            }
            @Override
            public void afterTextChanged(Editable s) {

                if (binding.editTextEuro.getText().toString().equalsIgnoreCase("NaN") || binding.editTextEuro.getText().toString().isEmpty()){
                    showToast("Please input a number");
                    binding.editTextEuro.setText("0");
                }else {
                    if(Double.valueOf(binding.editTextEuro.getText().toString()) != 0){ //Evitamos el valor 0 para que no nos de un resultado infinito
                        try{
                            if (binding.editTextEuro.hasFocus()){
                                setEuroAmount(Double.valueOf(binding.editTextEuro.getText().toString()));
                                binding.editTextDolar.setText(fromEuroToDolar());
                            }
                        }catch (NumberFormatException nfe){//Sacamos excepción por si en algún momento se setease el editText algún mensaje con caracteres diferentes de los numéricos.
                            showToast(nfe.getMessage());
                        }
                    }else
                    {
                        showToast("Introduzca un valor distinto de 0");
                    }
                }
            }
        });
    }

    private void showToast(String s){
        Toast.makeText(this, s, Toast.LENGTH_LONG).show();
    }

    public String fromDolarToEuro(){
        String result = Double.toString((Double.valueOf(getDolarAmount())) / (getRate()));

        return result;
    }

    public String fromEuroToDolar(){
        String result = Double.toString(((Double.valueOf(getEuroAmount())) * getRate()));
        return result;
    }

    public static double getRate() {
        return rate;
    }

    public static void setRate(double rate) {
        MainActivity.rate = rate;
    }

    public double getDolarAmount() {
        return dolarAmount;
    }

    public void setDolarAmount(double dolarAmount) {
        this.dolarAmount = dolarAmount;
    }

    public double getEuroAmount() {
        return euroAmount;
    }

    public void setEuroAmount(double euroAmount) {
        this.euroAmount = euroAmount;
    }
}