package com.iesportada.semipresencial.psp04conversorjson.bicizgz.adapter;

import android.view.View;

public interface ClickListener{
    public void onClick(View view, int position);

}