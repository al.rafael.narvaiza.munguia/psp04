package com.iesportada.semipresencial.psp04conversorjson.Conversor.network;

/**
 * @author Rafa Narvaiza
 * Clase DownloadIntentService. Se levanta un servicio que gestiona una descarga a través de OKhttp. Además, se escribe en memoria externa.
 */

import android.app.IntentService;
import android.content.Intent;
import android.os.Environment;
import android.util.Log;
import android.widget.Toast;

import com.iesportada.semipresencial.psp04conversorjson.Conversor.MainActivity;

import org.jetbrains.annotations.NotNull;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.ResponseBody;

public class DownloadIntentService extends IntentService {

    //Para que sdk detecte esta clase como servicio, además de extender IntentService, es necesario declararlo en el AndroidManifest.

    public DownloadIntentService() {
        super("DownloadIntentService");
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        String web = "";
        if (intent != null) {
            web = intent.getExtras().getString("web");
            URL url = null;
            try {
                url = new URL(web);
                descargaOkHTTP(url);
            } catch (MalformedURLException e) {
                e.printStackTrace();
                enviarRespuesta("Error en la URL: " + e.getMessage());
            }
        }
    }

    private void descargaOkHTTP(URL web) {
        final OkHttpClient client = new OkHttpClient();

        Request request = new Request.Builder()
                .url(web)
                .build();



        client.newCall(request).enqueue(new Callback() {

            @Override
            public void onFailure(@NotNull Call call, @NotNull IOException e) {


                Log.e("Error: ", e.getMessage());
                enviarRespuesta("Fallo: " + e.getMessage());
            }

            @Override
            public void onResponse(@NotNull Call call, @NotNull Response response) throws IOException {

                try (ResponseBody responseBody = response.body()) {
                    if (!response.isSuccessful()) {
                        //throw new IOException("Unexpected code " + response);
                        Log.e("Error: ", "Unexpected code " + response);
                        enviarRespuesta("Error: Unexpected code " + response);
                    } else {
                        // Read data on the worker thread
                        final String responseData = response.body().string();

                        // guardar el fichero descargado en memoria externa
                        if (escribirExterna(responseData)) {
                            Log.i("Descarga: ", "fichero descargado");
                        } else {
                            Log.e("Error ", "no se ha podido descargar");
                        }
                    }
                }
            }

        });
    }

    private void mostrarMensaje(String mensaje) {
        Toast.makeText(this,mensaje, Toast.LENGTH_SHORT).show();
    }

    //Escribimos un JSON en la SD y al mismo tiempo seteamos al método encargado de leer el JSON la ubicación de éste.

    private boolean escribirExterna(String cadena) {
        File miFichero, tarjeta;
        BufferedWriter bw = null;
        boolean correcto = false;
        try {
            tarjeta = Environment.getExternalStorageDirectory();
            miFichero = new File(tarjeta.getAbsolutePath(), "eurorates.json");
            bw = new BufferedWriter(new FileWriter(miFichero));
            bw.write(cadena);
            Log.i("Información: ", miFichero.getAbsolutePath());
            enviarRespuesta("Descarga: fichero descargado OK\n" + miFichero.getAbsolutePath());
        } catch (IOException e) {
            if (cadena != null)
                Log.e("Error: ", cadena);
            Log.e("Error de E/S", e.getMessage());
            enviarRespuesta("Error: " + e.getMessage());
        } finally {
            try {
                if (bw != null) {
                    bw.close();
                    correcto = true;
                }
            } catch (IOException e) {
                Log.e("Error al cerrar", e.getMessage());
            }
        }
        return correcto;
    }



    private void enviarRespuesta (String mensaje) {
        Intent i = new Intent();
        i.setAction(MainActivity.ACTION_RESP);
        i.addCategory(Intent.CATEGORY_DEFAULT);
        i.putExtra("resultado", mensaje);

        sendBroadcast(i);
    }

}
