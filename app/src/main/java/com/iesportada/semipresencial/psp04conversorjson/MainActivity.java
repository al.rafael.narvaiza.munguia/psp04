package com.iesportada.semipresencial.psp04conversorjson;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.iesportada.semipresencial.psp04conversorjson.databinding.ActivityMain2Binding;
import com.iesportada.semipresencial.psp04conversorjson.databinding.ActivityMainBinding;

public class MainActivity extends AppCompatActivity {

    private ActivityMain2Binding binding;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);
        binding = ActivityMain2Binding.inflate(getLayoutInflater());
        View v = binding.getRoot();
        setContentView(v);

        binding.buttonConversor.setOnClickListener(view -> {
            Intent intent = new Intent(this, com.iesportada.semipresencial.psp04conversorjson.Conversor.MainActivity.class);
            startActivity(intent);
        });
        binding.buttonParking.setOnClickListener(view -> {
            Intent intent = new Intent(this, com.iesportada.semipresencial.psp04conversorjson.bicizgz.MainActivity.class);
            startActivity(intent);
        });
    }
}