package com.iesportada.semipresencial.psp04conversorjson.bicizgz.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import java.util.ArrayList;

public class Listado {

    @SerializedName("result")
    @Expose
    private ArrayList<Result> result = null;

    public Listado(ArrayList<Result> result) {
        super();
        this.result = result;
    }
    public ArrayList<Result> getResult() {
        return result;
    }

}

