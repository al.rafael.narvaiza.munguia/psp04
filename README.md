# TO 04 - PSP

Configuración de servidor con:
- dominio propio redirigido a vps.
- Gestión de usuarios y desactivación de root desde ssh.
- Uso de postfix.
- Gestión de ufw.
- Gestión de fail2ban.
- Webmin&netdata.

Conversor de moneda que descargue un valor de conversión en json.
Aplicación que gestione las estaciones de bicicleta de Zaragoza con la información de su API REST.


# Conversor Json

Uso de librería Gson. Registro en https://exchangeratesapi.io/ para gestionar mis propias conversiones. Este servicio proporciona cada 24 horas un json con los cambios de una serie de monedas frente al euro. Es gratuita.

## APP Bicis Zaragoza

Uso de Gson con retrofit. Implementación en la actividad principal de un recyclerView encastrado en un swipeRefreshLayout para gestionar las actualizaciones de datos.
De cada una de las entradas del recyclerView se pasa a su detalle en una actividad detalle que implementa un formato de hora más comprensible.



